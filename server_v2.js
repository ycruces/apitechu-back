var express = require('express');
var user_file = require('./user.json');
var bodyParser = require('body-parser');
var glb = require('./global');
var users = require('./controllers/users');
const requestJSON = require('request-json');


var app = express();
var port = process.env.PORT ||  3000;
var apikeyMLab = "apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF";
const urlMLab = "https://api.mlab.com/api/1/databases/techu38db/collections/";
var httpClient = requestJSON.createClient(urlMLab);

app.use(bodyParser.json());

app.get(glb.URL_BASE + 'users', function (req, res) {
    console.log("Cliente HTTP mLab creado.");
     var queryString = 'f={"_id":0}&';
     httpClient.get('user?' + queryString + apikeyMLab,
     function(err, respuestaMLab, body) {
         var response = {};
         if(err) {
             response = {"msg" : "Error obteniendo usuario."}
             res.status(500);
         } else {
           if(body.length > 0) {
             console.log(body.length);
             response = body;
             res.status(202);
           } else {
             response = {"msg" : "Ningún elemento 'user'."}
             res.status(404);
           }
         }
         res.send(response);
       });
});


//lo mismo del bloque de arriba pero lamando a un modulo
//app.get(glb.URL_BASE + "users", users.getUsers);
//CTRL + SHIFT + / para comentar bloques

//peticion GET users con ID
app.get(glb.URL_BASE + "users/:id", function(req,res){
   var id = req.params.id;
   console.log("Cliente HTTP mLab GET id " + id);

   var queryString = 'q={"ID":' + id + '}&';
   var queryStrField = 'f={"_id":0}&';

   httpClient.get('user?' + queryString + queryStrField + apikeyMLab,

   function(err, respuestaMLab, body) {
       var response = {};
       if(err) {
           response = {"msg" : "Error obteniendo usuario."}
           res.status(500);
       } else {
         if(body.length > 0) {
           console.log(body.length);
           response = body;
           res.status(202);
         } else {
           response = {"msg" : "Ningún elemento 'user'."}
           res.status(404);
         }
       }
       res.send(response);
     });
});

app.get(glb.URL_BASE + "users/:id/accounts", function(req,res){
   var id = req.params.id;
   console.log("Cliente HTTP mLab creado con id " + id);

   var queryStringID = 'q={"id":' + id + '}&';
   //var queryString = 'q={"ID":' + id + '}&';
   var queryString= 'f={"_id":0}&';

   httpClient.get('account?' + queryString + queryStringID + apikeyMLab,

   function(err, respuestaMLab, body) {
       var response = {};
       if(err) {
           response = {"msg" : "Error obteniendo cuentas del usuario ." + id}
           res.status(500);
       } else {
         if(body.length > 0) {
           console.log(body.length);
           response = body;
           res.status(202);
         } else {
           response = {"msg" : "Ningún elemento en cuentas."}
           res.status(404);
         }
       }
       res.send(response);
     });
});

//GET users con Query String
// app.get(URL_BASE + "users", function (req, res) {
//   console.log(req.query.id);
//   console.log(req.query.name);
// });

//POST users
app.post(glb.URL_BASE + "users", function(req,res) {
  console.log(" body recibido " + req.body);
  console.log(" body recibido " + req.body.first_name);

  httpClient.get('user?' + apikeyMLab,
    function(err, respuestaMLab, body) {
      var response = {};
      if(err) {
          response = {"msg" : "Error obteniendo usuarios"}
          res.status(500);
          res.send(response);
      } else {
        if(body.length > 0) {
          console.log(body.length);

          let newID = body.length +1;
          let newUser = {
            "ID" : newID,
            "first_name": req.body.first_name,
            "last_name": req.body.last_name,
            "email":req.body.email,
            "password":req.body.password
          };
          //funciona colocando el urlMLab o sin
          httpClient.post(urlMLab + "user?" + apikeyMLab, newUser,
              function (err, respuestaMLab, body){
                response = {"msg" : "Usuario insertado."}
                res.status(200);
                res.send(response);
              }
          );

        } else {
          response = {"msg" : "Ningún elemento en usuarios."}
          res.status(404);
          res.send(response);
        }
      }
      //
    });
});

//PUT users

app.put(glb.URL_BASE + "users/:id", function(req,res) {
  console.log("PUT => id = " + req.params.id);
  console.log("PUT => body " + req.body.first_name);
  console.log("PUT => body " + req.body.last_name);
  console.log("PUT => body " + req.body.email);
  console.log("PUT => body " + req.body.password);

  var id = req.params.id;
  var queryStringID = 'q={"ID":' + id + '}&';
  httpClient.get('user?'+ queryStringID + apikeyMLab,
    function(error, respuestaMLab, body) {
     var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
     console.log(req.body);
     console.log(cambio);
     httpClient.put('user?' + queryStringID + apikeyMLab, JSON.parse(cambio),
      function(error, respuestaMLab, body) {
        console.log("PUT error " + error);
        console.log("PUT body:"+ body); // body.n devuelve 1 si pudo hacer el update
       //res.status(200).send(body);
       res.send(body);
      });
    });
});

/*
//PUT users 2da forma
app.put(glb.URL_BASE + "users/:id", function(req,res) {
  console.log("PUT => id = " + req.params.id);
  console.log("PUT => body " + req.body.first_name);
  console.log("PUT => body " + req.body.last_name);
  console.log("PUT => body " + req.body.email);
  console.log("PUT => body " + req.body.password);

  var id = req.params.id;
  let userBody = req.body;
  var queryString = 'q={"ID":' + id + '}&';

  httpClient.get('user?' + queryString + apikeyMLab,
    function(err, respuestaMLab, body){
      let response = body[0];
      console.log("GET " + body);
      //Actualizo campos del usuario

      let updatedUser = {
        "ID" : id,
        "first_name" : req.body.first_name,
        "last_name" : req.body.last_name,
        "email" : req.body.email,
        "password" : req.body.password
      };//Otra forma simplificada (para muchas propiedades)
      // var updatedUser = {};
      // Object.keys(response).forEach(key => updatedUser[key] = response[key]);
      // Object.keys(userBody).forEach(key => updatedUser[key] = userBody[key]);
      // PUT a mLab
      httpClient.put('user/' + response._id.$oid + '?' + apikeyMLab, updatedUser,
        function(err, respuestaMLab, body){
          var response = {};
          if(err) {
              response = {
                "msg" : "Error actualizando usuario."
              }
              res.status(500);
          } else {
            if(body.length > 0) {
              response = body;
            } else {
              response = {
                "msg" : "Usuario actualizado correctamente."
              }
              res.status(200);
            }
          }

          res.send(response);
        });
    });
});
*/
// DELETE users
app.delete(glb.URL_BASE + "users/:id", function(req,res) {
  console.log("Delete => id " + req.params.id);
  let pos = req.params.id -1;

  if  (user_file[pos] == undefined) {
    res.send({"msg":"Usuario a eliminar no encontrado"});
  }
  else {
    user_file.splice(pos,1);
    res.send({"msg":"Usuario eliminado"});
  }
});

//login para hacer login se hace generalmente se usa POST
app.post(glb.URL_BASE + "login",function(req,res) {
  let encontro = false;
  let respuesta =  {"msg":"Password Incorrecto"};
  console.log(" user =  " + req.body.email + " passw = " + req.body.password);
  for (let i = 0; i < user_file.length && encontro == false; i++) {
    if (user_file[i].email == req.body.email && user_file[i].password == req.body.password) {
      respuesta = {"msg":"Password correcto"};
      user_file[i].logged = true;
      encontro = true;
      console.log(user_file);
    }
  }
  res.send(respuesta);
});


//logout  2do entregable
//delete user.logged;
//login para hacer login se hace generalmente se usa POST
app.post(glb.URL_BASE + "logout",function(req,res) {
  let encontro = false;
  let respuesta =  {"msg":"Usuario no existe"};
  console.log(" user =  " + req.body.email + " passw = " + req.body.password);
  for (let i = 0; i < user_file.length && encontro == false; i++) {
    if (user_file[i].email == req.body.email && user_file[i].password == req.body.password) {
      respuesta = {"msg":"Usuario deslogeado correctamente"};
      delete user_file[i].logged;  //borramos la propiedad logged del objeto
      encontro = true;
      console.log(user_file);
    }
  }
  res.send(respuesta);
});

//Get users logged
app.get(glb.URL_BASE + "getLogged", function (req, res) {
  console.log(user_file[0].first_name);
  /*
  var userLogged = {
    first_name:"",
    last_name:""
  };
  var userLogged = new Object(); // no funciona correctamente porque el ultimo objeto reescribe a los anteriores
  */
  var usersLogged = [];
  for (let i = 0; i < user_file.length ; i++) {
    console.log(user_file[i].logged);
    if (user_file[i].logged != undefined && user_file[i].logged == true){
      let userLogged ={};

      userLogged.first_name = user_file[i].first_name;
      userLogged.last_name = user_file[i].last_name;
      usersLogged.push(userLogged);
    }
  }
  if ( usersLogged.length> 0 )
    res.send(usersLogged);
  else
    res.send({"msg":"No hay usuarios logeados"});
  console.log(usersLogged);

});


app.listen(port, function () {
  console.log('Servidor escuchando en el puerto : ' + port);
});

function writeUserDataToFile(data) {
   var fs = require('fs');
   var jsonUserData = JSON.stringify(data);
   fs.writeFile("./users.json", jsonUserData, "utf8",
    function(err) { //función manejadora para gestionar errores de escritura
      if(err) {
        console.log(err);
      } else {
        console.log("Datos escritos en 'users.json'.");
      }
    })
}
