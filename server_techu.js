var express = require ('express');
var jwt = require('jsonwebtoken');
var bodyParser = require('body-parser');

var glb = require('./global_tar');

console.log("paso 1");
var ordenRoute = require('./routes/orden_route');
var stockRoute = require('./routes/stock_route');

console.log("paso 2");
require('dotenv').config();

const cors = require('cors');

console.log("paso 3");
var app = express();
var port = process.env.PORT ||  3000;

app.use(cors());

app.use(bodyParser.json({limit:'50mb'}));
app.use(bodyParser.urlencoded({limit:'50mb',extended: true}));
//app.use(bodyParser.json());

//*****Lineas agregadas para que el NAVEGADOR no bloquee las peticiones al MLAB (otro dominio)
//*****por defecto*****//
app.options("*",cors());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

console.log("paso 10 "+  glb.URL_BASE);
//Controllers
app.use(glb.URL_BASE +'/orden',ordenRoute);
app.use(glb.URL_BASE +'/stock',stockRoute);

app.listen(port, function () {
  console.log('Servidor escuchando en el puerto : ' + port);
});
