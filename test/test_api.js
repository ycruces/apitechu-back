var mocha = require("mocha"); // JavaScript Test Framework
var chai = require("chai"); // Aserciones
var chaihttp = require("chai-http"); // Peticiones HTTP
//Aumentamos la funcionalidad de chai, con este plugin para poder lanzar peticiones
//http
chai.use(chaihttp);
var should = chai.should();
const expect = require('chai').expect; // esto es para usar expect, si no se pone establecemos
                                      // sentencia fallaria la ultima prueba donde se esta usando expect.

//Preparo el entorno para que podamos probarlo.
//var server = require('../server_v2');
describe('First test suite',//Suite de test unitario
   function(){//Funcion manejadora de una suite
     it ('Test that DuckDuckGo works', //Test unitario
       function(done){
           chai.request('http://www.DuckDuckGo.com')//Usamos Chai para hacer la api, asi establecemos el dominio base
               .get('/')// Establecemos una prueba para get al metodo /
               .end(//Una vez que se ha hecho la petición, se lanza el end y en el vemos el resultado.
                 function(err, res){//Aqui vamos a esablecer las aserciones en funcion del resultado de la petición.
                   console.log("Resquest has ended");
                   console.log(err);
                   //console.log(res);
                   res.should.have.status(200);
                   done();//Le decimos al framework que hemos terminado para evaluar las aserciones.
                 })
       })
   });
 describe('Test de API TechU',//Suite de test unitario
    function(){//Funcion manejadora de una suite
     //  it ('Prueba que la API funciona correctamente', //Test unitario
     //    function(done){
     //        chai.request('http://localhost:3000')//Usamos Chai para hacer la api, asi establecemos el dominio base
     //           // .get('/apitechu/v1')// Establecemos una prueba para get al metodo /
     //            .get('/colapi/v3')// Establecemos una prueba para get al metodo /
     //            .end(//Una vez que se ha hecho la petición, se lanza el end y en el vemos el restultado.
     //              function(err, res){//Aqui vamos a esablecer las aserciones en funcion del resultado de la petición.
     //                console.log("Resquest has ended");
     //                console.log(err);
     //                //console.log(res);
     //                res.should.have.status(200);
     //                res.body.msg.should.be.eql("hola desde colapi/v3");
     //                done();//Le decimos al framework que hemos terminado para evaluar las aserciones.
     //              })
     //    }
     //  ),
      it ('Prueba que la API devuelve una lista de usuarios correctos', //Test unitario
        function(done){
            chai.request('http://localhost:3000')//Usamos Chai para hacer la api, asi establecemos el dominio base
                .get('/techu-peru/v1/users')// Establecemos una prueba para get al metodo /
                // .get('/apitechu/v1/users')// Establecemos una prueba para get al metodo /
                .end(//Una vez que se ha hecho la petición, se lanza el end y en el vemos el restultado.
                  function(err, res){//Aqui vamos a esablecer las aserciones en funcion del resultado de la petición.
                    console.log("Resquest has ended");
                    console.log(err);
                    //console.log(res);
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    for (users of res.body){
                      users.should.have.property('first_name');
                      users.should.have.property('last_name');
                     //  users.should.have.property('email');
                     //  users.should.have.property('password');
                    }
                    done();//Le decimos al framework que hemos terminado para evaluar las aserciones.
                  })
        })
    });
describe('get the users with id 1: ',()=>{
	it('should get the users with id 1', (done) => {
    chai.request("http://localhost:3000")
      .get('/techu-peru/v1/users/1')
      .end( function(err,res){
        console.log(res.body)
        expect(res.body).to.have.property('ID').to.be.equal(1);
        expect(res).to.have.status(200);
        done();
      });
  });
});
//mas info podemos obtenerlo de
//https://www.paradigmadigital.com/dev/testeo-api-rest-mocha-chai-http/?utm_source=Site&utm_campaign=Related_Posts_Right&utm_medium=Blog
