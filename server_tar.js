var express = require('express');
var bodyParser = require('body-parser');
var glb = require('./global_tar');
var oficinas = require('./controllers/oficinas');
var cors = require('cors');

var app = express();
var port = process.env.PORT ||  3000;

app.use(bodyParser.json());

app.use(cors());
app.options("*",cors());


// GET oficinas
app.get(glb.URL_BASE + "oficinas", oficinas.getOficinas);

// GET oficina by code
app.get(glb.URL_BASE + "oficinas/:id", oficinas.getOficina);

//POST oficinas
app.post(glb.URL_BASE + "oficinas", oficinas.insertOficinas);

//PUT oficinas
app.put(glb.URL_BASE + "oficinas", oficinas.updateOficinas);

// DELETE oficinas
app.delete(glb.URL_BASE + "oficinas/:id", oficinas.deleteOficinas);


app.listen(port, function () {
  console.log('Servidor escuchando en el puerto : ' + port);
});
