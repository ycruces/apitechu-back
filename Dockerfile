#imagen docker base/inicial
FROM node:latest

#crear el directorio del contenedor docker
WORKDIR /docker-apitechu

#copia archivos del proyeto en el directorio del contenedor
ADD . /docker-apitechu

#exponer puerto de escucha del contenedor (mismo definido en nuestra API)
EXPOSE 3000

#lanzar comandos para ejecutar nuestra app
CMD ["npm","run","start-dev"]
