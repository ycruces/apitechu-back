var express = require('express');
var router = express.Router();
var middleware = require('../util/middleware')
require('dotenv').config();

var orden_controller = require('../controllers/orden_controller');

router.get('/',middleware.ensureAuthenticated, orden_controller.getOrdenes);
router.get('/:id',middleware.ensureAuthenticated, orden_controller.getOrden);

router.post('/:id',middleware.ensureAuthenticated, orden_controller.insertOrden);
router.put('/:id',middleware.ensureAuthenticated, orden_controller.updateOrden);
router.delete('/:id',middleware.ensureAuthenticated, orden_controller.deleteOrden);

//router.get('/:id',middleware.ensureAuthenticated, orden_controller.getOrdenxTarjeta);
//router.get('/:id',middleware.ensureAuthenticated, orden_controller.getOrdenxOficina);
//router.post('/',middleware.ensureAuthenticated, orden_controller.getOrdenxOfiyNroOrden);
//router.post('/',middleware.ensureAuthenticated, orden_controller.getOrdenxOfiyTipoTar);

router.post('/',middleware.ensureAuthenticated, orden_controller.getOrdenxOfiyNroOrdenyTipoTar);

module.exports = router;
