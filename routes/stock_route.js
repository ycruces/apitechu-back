var express = require('express');
var router = express.Router();
var middleware = require('../util/middleware')
require('dotenv').config();

var stock_controller = require('../controllers/stock_controller');

router.get('/',middleware.ensureAuthenticated, stock_controller.getStock);

router.get('/:id',middleware.ensureAuthenticated, stock_controller.getStockxOficina);
//router.post('/',middleware.ensureAuthenticated, stock_controller.getStockxTarjeta);

router.post('/',middleware.ensureAuthenticated, stock_controller.getStockxOfiyTipoTar);

module.exports = router;
