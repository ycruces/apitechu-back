var express = require('express');
var user_file = require('./user.json');
var bodyParser = require('body-parser');
var glb = require('./global');
var users = require('./controllers/users');

var app = express();
var port = process.env.PORT ||  3000;

app.use(bodyParser.json());

/*
app.get(global.URL_BASE + 'users', function (req, res) {
  res.status(202).send(user_file);
});
*/
//lamando a un modulo
app.get(glb.URL_BASE + "users", users.getUsers);
//CTRL + SHIFT + / para comentar bloques

//peticion GET users con ID
app.get(glb.URL_BASE + "users/:id", function(req,res){
  let pos = req.params.id -1;
  let size = user_file.length;
  console.log(" Tamaño del arreglo " + size);
  console.log("Get con id = " + req.params.id);
  let respuesta = user_file[pos] == undefined?
    {"msg":"Usuario no encontrado"}:user_file[pos];
  res.send(respuesta) ;
});

//GET users con Query String
// app.get(URL_BASE + "users", function (req, res) {
//   console.log(req.query.id);
//   console.log(req.query.name);
// });

//POST users
app.post(glb.URL_BASE + "users", function(req,res) {
  console.log(" body recibido " + req.body);
  console.log(" body recibido " + req.body.first_name);
  let newID = user_file.length +1;
  let newUser = {
    "ID" : newID,
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email":req.body.email,
    "password":req.body.password
  };
  user_file.push(newUser);
  res.send(newUser);
  console.log(user_file);

});

//PUT users
app.put(glb.URL_BASE + "users/:id", function(req,res) {
  console.log("PUT => id = " + req.params.id);
  console.log("PUT => body " + req.body.first_name);
  console.log("PUT => body " + req.body.last_name);
  console.log("PUT => body " + req.body.email);
  console.log("PUT => body " + req.body.password);

  if (user_file[req.params.id -1] == undefined) {
    res.send({"msg":"Usuario no encontrado"});
  }
  else {
    let user = {
    "ID" : req.params.id,
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email":req.body.email,
    "password":req.body.password
    };
    user_file[req.params.id-1] = user;
    res.send(user);
    console.log(user_file);
  }
});

// DELETE users
app.delete(glb.URL_BASE + "users/:id", function(req,res) {
  console.log("Delete => id " + req.params.id);
  let pos = req.params.id -1;

  if  (user_file[pos] == undefined) {
    res.send({"msg":"Usuario a eliminar no encontrado"});
  }
  else {
    user_file.splice(pos,1);
    res.send({"msg":"Usuario eliminado"});
  }
});

//login para hacer login se hace generalmente se usa POST
app.post(glb.URL_BASE + "login",function(req,res) {
  let encontro = false;
  let respuesta =  {"msg":"Password Incorrecto"};
  console.log(" user =  " + req.body.email + " passw = " + req.body.password);
  for (let i = 0; i < user_file.length && encontro == false; i++) {
    if (user_file[i].email == req.body.email && user_file[i].password == req.body.password) {
      respuesta = {"msg":"Password correcto"};
      user_file[i].logged = true;
      encontro = true;
      console.log(user_file);
    }
  }
  res.send(respuesta);
});


//logout  2do entregable
//delete user.logged;
//login para hacer login se hace generalmente se usa POST
app.post(glb.URL_BASE + "logout",function(req,res) {
  let encontro = false;
  let respuesta =  {"msg":"Usuario no existe"};
  console.log(" user =  " + req.body.email + " passw = " + req.body.password);
  for (let i = 0; i < user_file.length && encontro == false; i++) {
    if (user_file[i].email == req.body.email && user_file[i].password == req.body.password) {
      respuesta = {"msg":"Usuario deslogeado correctamente"};
      delete user_file[i].logged;  //borramos la propiedad logged del objeto
      encontro = true;
      console.log(user_file);
    }
  }
  res.send(respuesta);
});

//Get users logged
app.get(glb.URL_BASE + "getLogged", function (req, res) {
  console.log(user_file[0].first_name);
  /*
  var userLogged = {
    first_name:"",
    last_name:""
  };
  var userLogged = new Object(); // no funciona correctamente porque el ultimo objeto reescribe a los anteriores
  */
  var usersLogged = [];
  for (let i = 0; i < user_file.length ; i++) {
    console.log(user_file[i].logged);
    if (user_file[i].logged != undefined && user_file[i].logged == true){
      let userLogged ={};

      userLogged.first_name = user_file[i].first_name;
      userLogged.last_name = user_file[i].last_name;
      usersLogged.push(userLogged);
    }
  }
  if ( usersLogged.length> 0 )
    res.send(usersLogged);
  else
    res.send({"msg":"No hay usuarios logeados"});
  console.log(usersLogged);

});


app.listen(port, function () {
  console.log('Servidor escuchando en el puerto : ' + port);
});

function writeUserDataToFile(data) {
   var fs = require('fs');
   var jsonUserData = JSON.stringify(data);
   fs.writeFile("./users.json", jsonUserData, "utf8",
    function(err) { //función manejadora para gestionar errores de escritura
      if(err) {
        console.log(err);
      } else {
        console.log("Datos escritos en 'users.json'.");
      }
    })
}
