const  URL_BASE = "/techu-peru/v1/"
const requestJSON = require('request-json');
const urlMLab = "https://api.mlab.com/api/1/databases/techu38db/collections/";
var httpClient = requestJSON.createClient(urlMLab);
console.log("Cliente HTTP mLab creado. " + httpClient);

module.exports = {
  URL_BASE,
  httpClient
}
