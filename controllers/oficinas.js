const glb = require('../global_tar');
require("dotenv").config();
const apikeyMLab = "apiKey="+ process.env.MLAB_API_KEY;

function getOficinas(req,res) {
   var queryString = 'f={"_id":0}&';

   glb.httpClient.get('oficinas?' + queryString + apikeyMLab,
   function(err, respuestaMLab, body) {
       var response = {};
       if(err) {
           response = {"msg" : "Error obteniendo la Oficina."}
           res.status(500);
       } else {
         if(body.length > 0) {
           console.log(body.length);
           response = body;
         } else {
           response = {"msg" : "Oficina no existe"}
           res.status(404);
         }
       }
       res.send(response);
     });
}

function getOficina(req,res) {
  var cod_ofi = req.params.id;
  console.log("Cliente HTTP mLab GET id " + cod_ofi);

  var queryString = 'q={"CODIGO_OFICINA":"'  + cod_ofi + '"}&';
  console.log("queryString => " + queryString);
  var queryStrField = 'f={"_id":0}&';

  glb.httpClient.get('oficinas?' + queryString + queryStrField + apikeyMLab,

  function(err, respuestaMLab, body) {
      var response = {};
      if(err) {
          response = {"msg" : "Error obteniendo datos de la oficina."}
          res.status(500);
      } else {
        if(body.length > 0) {
          console.log(body.length);
          response = body;
        } else {
          response = {"msg" : "No existe la oficina solicitada"}
          res.status(404);
        }
      }
      res.send(response);
    });
}


function insertOficinas(req, res) {

  var cod_ofi = req.body.CODIGO_OFICINA;
  console.log("Cliente HTTP mLab GET id " + cod_ofi);

  var queryString = 'q={"CODIGO_OFICINA":"'  + cod_ofi + '"}&';
  var queryStrField = 'f={"_id":0}&';

  glb.httpClient.get('oficinas?' + queryString + queryStrField + apikeyMLab,
    function(err, respuestaMLab, body) {
      var response = {};
      if(err) {
          response = {"msg" : "Error obteniendo oficina"}
          res.status(500);
          res.send(response);
      } else {
        console.log(" body.length " + body.length);
        if(body.length <= 0) { //no existe el registro por lo que se puede insertar
          let newOfi = {
            "CODIGO_OFICINA" : req.body.CODIGO_OFICINA,
            "DESCRIPCION_OFICINA": req.body.DESCRIPCION_OFICINA,
            "UBIGEO":req.body.UBIGEO
          };
          glb.httpClient.post("oficinas?" + apikeyMLab, newOfi,
              function (err, respuestaMLab, body){
                response = {"msg" : "Oficina insertada OK."}
                res.status(200);
                res.send(response);
              }
          );
        } else {
          response = {"msg" : "No se puede insertar la oficina, porque ya existe."}
          res.status(404);
          res.send(response);
        }
      }
    });
}

function updateOficinas(req, res){
  var cod_ofi = req.body.CODIGO_OFICINA;
  var queryStringID = 'q={"CODIGO_OFICINA":"' + cod_ofi + '"}&';
  glb.httpClient.get('oficinas?'+ queryStringID + apikeyMLab,
    function(error, respuestaMLab, body) {

      var response = {};
      if(error) {
        response = {"msg" : "Error no existe la oficina para actualizar"}
        res.status(500);
        res.send(response);
      } else {
        console.log(" body.length " + body.length);
        if(body.length > 0) { //SI existe el registro por lo que se puede actualizar
          var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
          console.log(req.body);
          console.log(cambio);
          glb.httpClient.put('oficinas?' + queryStringID + apikeyMLab, JSON.parse(cambio),
            function(error, respuestaMLab, body) {
              console.log(" Registros actualizados " + body.n)
              if (body.n > 0) {
                response = {"msg": "Registro Insertado"};
                res.status(200);
              }
              else {
                res.status(404);
                response = {"msg": "No se pudo insertar la oficina"};
              }
              res.send(response);
          });
        }
        else {
          response = {"msg" : "Error no existe oficina a actualizar"}
          res.status(404);
          res.send(response);
        }
      }
    });
}

function deleteOficinas(req, res) {
  console.log("Delete => oficina " + req.params.id);

  var code_ofi = req.params.id;
   var queryStringID = 'q={"CODIGO_OFICINA":"' + code_ofi + '"}&';
   glb.httpClient.get('oficinas?' +  queryStringID + apikeyMLab,
     function(error, respuestaMLab, body){
       if (!error && body.length > 0) {
         var respuesta = body[0];
         glb.httpClient.delete("oficinas/" + respuesta._id.$oid +'?'+ apikeyMLab,
            function(error, respuestaMLab,body){
              res.send({"msg":"Oficina eliminiada OK"});
         });
       }
       else {
         res.status(404).send({"msg":"Codigo de Oficina no valido"});
       }
     });
}

module.exports.getOficinas = getOficinas;
module.exports.getOficina = getOficina;
module.exports.insertOficinas = insertOficinas;
module.exports.updateOficinas = updateOficinas;
module.exports.deleteOficinas = deleteOficinas;
