const glb = require('../global_tar');
require("dotenv").config();
const apikeyMLab = "apiKey="+ process.env.MLAB_API_KEY;

exports.getStock = function (req, res) {
   var queryString = 'f={"_id":0}&';

   glb.httpClient.get('stock?' + queryString + apikeyMLab,
   function(err, respuestaMLab, body) {
       var response = {};
       if(err) {
           response = {"msg" : "Error obteniendo el Stock."};
           res.status(500);
       } else {
         if(body.length > 0) {
           console.log(body.length);
           response = body;
         } else {
           response = {"msg" : "No existe datos de Stock"}
           res.status(404);
         }
       }
       res.send(response);
     });
}


exports.getStockxOficina = function(req,res) {
  var nro_ofi = req.params.id;
  console.log("Cliente HTTP mLab GET nro Oficina " + nro_ofi);

  var queryString = 'q={"CODIGO_OFICINA":"'  + nro_ofi + '"}&';
  console.log("{getStockxOficina} queryString => " + queryString);
  var queryStrField = 'f={"_id":0}&';

  glb.httpClient.get('stock?' + queryString + queryStrField + apikeyMLab,

  function(err, respuestaMLab, body) {
      var response = {};
      if(err) {
          response = {"msg" : "Error obteniendo datos del Stock por oficina."}
          res.status(500);
      } else {
        if(body.length > 0) {
          console.log(body.length);
          response = body;
        } else {
          response = {"msg" : "No existe datos solicitados"}
          res.status(404);
        }
      }
      res.send(response);
    });
}
/*
exports.getStockxTarjeta =function(req,res) {
  var tipo_tar = req.body.TIPO_TARJETA;
  console.log("Cliente HTTP mLab GET tipo_tar " + tipo_tar);

  var queryString = 'q={"TIPO_TARJETA":"'  + tipo_tar + '"}&';
  console.log("queryString => " + queryString);
  var queryStrField = 'f={"_id":0}&';

  glb.httpClient.get('stock?' + queryString + queryStrField + apikeyMLab,

  function(err, respuestaMLab, body) {
      var response = {};
      if(err) {
          response = {"msg" : "Error obteniendo datos del Sotck por tipo de tarjeta."}
          res.status(500);
      } else {
        if(body.length > 0) {
          console.log(body.length);
          response = body;
        } else {
          response = {"msg" : "No existe datos del Stock por tipo de tarjeta solicitada"}
          res.status(404);
        }
      }
      res.send(response);
    });
}
*/

exports.getStockxOfiyTipoTar = function(req,res) {

  var tipo_tar = req.body.TIPO_TARJETA;
  var nro_ofi = req.body.CODIGO_OFICINA;

  let queryString = "";
  let error = false;

  if (tipo_tar == undefined && nro_ofi == undefined) {
    response = {"msg" : "Debe ingresar un criterio de busqueda"}
    res.status(500);
    error = true;
    res.send(response);
  }
  else if (tipo_tar == undefined) {
    queryString = 'q={"CODIGO_OFICINA":"' + nro_ofi + '"}&';
  }
  else if (nro_ofi == undefined) {
    queryString = 'q={"TIPO_TARJETA":"' + tipo_tar +'"}&';
  }
  else {
    queryString = 'q={"CODIGO_OFICINA":"' + nro_ofi + '","TIPO_TARJETA":"' + tipo_tar +'"}&';
  }

  if (!error) {
    console.log("{getStockxOfiyTipoTar} queryString => " + queryString);

    var queryStrField = 'f={"_id":0}&';

    console.log(queryString + queryStrField + apikeyMLab);

    glb.httpClient.get('stock?' + queryString + queryStrField + apikeyMLab,

    function(err, respuestaMLab, body) {
      var response = {};
      if(err) {
          response = {"msg" : "Error obteniendo datos Solicitados."}
          res.status(500);
      } else {
        if(body.length > 0) {
          console.log(body.length);
          response = body;
        } else {
          response = {"msg" : "No existe datos por el filtro solicitado"}
          res.status(404);
        }
      }
      res.send(response);
    });
  }
}
