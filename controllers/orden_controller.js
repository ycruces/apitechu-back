const glb = require('../global_tar');
require("dotenv").config();
const apikeyMLab = "apiKey="+ process.env.MLAB_API_KEY;

exports.getOrdenes = function(req,res) {
   var queryString = 'f={"_id":0}&';

   console.log("[getOrdenes] " + queryString + apikeyMLab);
   glb.httpClient.get('ordenes?' + queryString + apikeyMLab,
   function(err, respuestaMLab, body) {
       var response = {};
       if(err) {
           response = {"msg" : "Error obteniendo las ordenes."}
           res.status(500);
       } else {
         if(body.length > 0) {
           console.log(body.length);
           response = body;
         } else {
           response = {"msg" : "No existe ordenes a procesar"}
           res.status(404);
         }
       }
       res.send(response);
     });
}

exports.getOrden = function(req,res) {
  var nro_orden = req.params.id;
  console.log("[getOrden] nro Orden " + nro_orden);

  //var queryString = 'q={"idorden":"'  + nro_orden + '"}&'; //string
  var queryString = 'q={"nro_orden":'  + nro_orden + '}&'; //int32
  console.log("queryString => " + queryString);
  var queryStrField = 'f={"_id":0}&';

  glb.httpClient.get('ordenes?' + queryString + queryStrField + apikeyMLab,

  function(err, respuestaMLab, body) {
      var response = {};
      if(err) {
          response = {"msg" : "Error obteniendo datos de la Orden Solicitada."}
          res.status(500);
      } else {
        if(body.length > 0) {
          console.log(body.length);
          response = body;
        } else {
          response = {"msg" : "No existe la Orden solicitada"}
          res.status(404);
        }
      }
      res.send(response);
    });
}

exports.getOrdenxOfiyNroOrdenyTipoTar = function(req,res) {

  var tipo_tar = req.body.tipo_tarjeta;
  var nro_orden = req.body.nro_orden;
  var nro_ofi = req.body.oficina_solicitante;
  let error = false;
  let queryString ="";

  if (tipo_tar == undefined && nro_ofi == undefined && nro_orden == undefined) {
    response = {"msg" : "Debe ingresar un criterio de busqueda"}
    res.status(500);
    error = true;
    res.send(response);
  }
  else if (tipo_tar == undefined && nro_orden == undefined) {
    queryString = 'q={"oficina_solicitante":"' + nro_ofi + '"}&';
  }
  else if (nro_ofi == undefined && nro_orden == undefined) {
    queryString = 'q={"tipo_tarjeta":"' + tipo_tar +'"}&';
  }
  else if (tipo_tar == undefined && nro_ofi == undefined) {
    queryString = 'q={"nro_orden":' + nro_orden +'}&';
  }
  else if (tipo_tar == undefined ) {
    queryString = 'q={"oficina_solicitante":"' + nro_ofi + '","nro_orden":' + nro_orden +'}&';
  }
  else if (nro_ofi == undefined ) {
    queryString = 'q={"tipo_tarjeta":"' + tipo_tar + '","nro_orden":' + nro_orden +'}&';
  }
  else if (nro_orden == undefined) {
    queryString = 'q={"oficina_solicitante":"' + nro_ofi + '","tipo_tarjeta":"' + tipo_tar +'"}&';
  }
  else {
    queryString = 'q={"oficina_solicitante":"' + nro_ofi + '","tipo_tarjeta":"' + tipo_tar +'","nro_orden":' + nro_orden +'}&';
  }

  if (!error) {
    console.log("{getStockxOfiyTipoTar} queryString => " + queryString);
    var queryStrField = 'f={"_id":0}&';

    glb.httpClient.get('ordenes?' + queryString + queryStrField + apikeyMLab,
    function(err, respuestaMLab, body) {
      var response = {};
      if(err) {
          response = {"msg" : "Error obteniendo datos Solicitados."}
          res.status(500);
      } else {
        if(body.length > 0) {
          console.log(body.length);
          response = body;
        } else {
          response = {"msg" : "No existe datos por el filtro solicitado"}
          res.status(404);
        }
      }
      res.send(response);
    });
  }
}

exports.insertOrden = function(req, res) {

  var nro_orden = req.body.nro_orden;
  console.log("[insertOrden] nro_orden " + nro_orden);

  var queryString = 'q={"nro_orden":'  + nro_orden + '}&';
  var queryStrField = 'f={"_id":0}&';

  glb.httpClient.get('ordenes?' + queryString + queryStrField + apikeyMLab,
    function(err, respuestaMLab, body) {
      var response = {};
      if(err) {
          response = {"msg" : "Error en el servicio al insertar una Orden"}
          res.status(500);
          res.send(response);
      } else {
        console.log(" body.length " + body.length);
        if(body.length <= 0) { //no existe el registro por lo que se puede insertar
          let newOrden = {
            "nro_orden" : req.body.nro_orden,
            "oficina_solicitante": req.body.oficina_solicitante,
            "tipo_tarjeta": req.body.tipo_tarjeta,
            "formato_tarjeta": req.body.formato_tarjeta,
            "nro_atm_comercio": req.body.nro_atm_comercio,
            "diseno_tarjeta": req.body.diseno_tarjeta,
            "can_tarjetasord": req.body.can_tarjetasord,
            "can_tarjetasrec": req.body.can_tarjetasrec,
            "situacion_orden": req.body.situacion_orden,
            "fecha_alta": req.body.fecha_alta,
            "hora_alta": req.body.hora_alta,
            "usuario_alta": req.body.usuario_alta,
            "fecha_estampacion": req.body.fecha_estampacion,
            "hora_estampacion": req.body.hora_estampacion,
            "usuario_estampacion": req.body.usuario_estampacion,
            "fecha_anulacion": req.body.fecha_anulacion,
            "hora_anulacion": req.body.hora_anulacion,
            "usuario_anulacion": req.body.usuario_anulacion,
            "leyenda": req.body.leyenda,
            "usuario_actualizacion": req.body.usuario_actualizacion,
            "terminal_actualizacion": req.body.terminal_actualizacion,
            "timestamp":req.body.timestamp,
            "numero_cajetin":req.body.numero_cajetin
          };
          glb.httpClient.post("ordenes?" + apikeyMLab, newOrden,
              function (err, respuestaMLab, body){
                response = {"msg" : "Orden insertada OK."}
                res.status(200);
                res.send(response);
              }
          );
        } else {
          response = {"msg" : "No se puede insertar la Orden, porque ya existe."}
          res.status(404);
          res.send(response);
        }
      }
    });
}

exports.updateOrden = function(req, res){
  var nro_orden = req.body.nro_orden;
  console.log("[updateOrden] nro_orden " + nro_orden);

  var queryStringID = 'q={"nro_orden":' + nro_orden + '}&';
  glb.httpClient.get('ordenes?'+ queryStringID + apikeyMLab,
    function(error, respuestaMLab, body) {

      var response = {};
      if(error) {
        response = {"msg" : "Error en el proceso de actualizacion"}
        res.status(500);
        res.send(response);
      } else {
        console.log(" body.length " + body.length);
        if(body.length > 0) { //SI existe el registro por lo que se puede actualizar
          var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
          console.log(req.body);
          console.log(cambio);
          glb.httpClient.put('ordenes?' + queryStringID + apikeyMLab, JSON.parse(cambio),
            function(error, respuestaMLab, body) {
              console.log(" Registros actualizados " + body.n)
              if (body.n > 0) {
                response = {"msg": "Registro Actualizado"};
                res.status(200);
              }
              else {
                res.status(404);
                response = {"msg": "No se pudo actualizar la Orden"};
              }
              res.send(response);
          });
        }
        else {
          response = {"msg" : "No existe documento para actualizar"}
          res.status(404);
          res.send(response);
        }
      }
    });
}

exports.deleteOrden = function(req, res) {
  var nro_orden = req.params.id;
  console.log("[deleteOrden] => orden a eliminar " + nro_orden);

  var queryStringID = 'q={"nro_orden":' + nro_orden + '}&';

  glb.httpClient.get('ordenes?' +  queryStringID + apikeyMLab,
     function(error, respuestaMLab, body){
       if (!error && body.length > 0) {
         var respuesta = body[0];
         glb.httpClient.delete("ordenes/" + respuesta._id.$oid +'?'+ apikeyMLab,
            function(error, respuestaMLab,body){
              res.send({"msg":"Orden eliminada OK"});
         });
       }
       else {
         res.status(404).send({"msg":"Nro de Orden no valido"});
       }
     });
}
