const glb = require('../global');
/* esto funciona cuando no se declara el httpClient en global
const requestJSON = require('request-json');
const urlMLab = "https://api.mlab.com/api/1/databases/techu38db/collections/";
var httpClient = requestJSON.createClient(urlMLab);
console.log("Cliente HTTP mLab creado. " + httpClient);
*/
require("dotenv").config();
const apikeyMLab = "apiKey="+ process.env.MLAB_API_KEY;

function getUsers(req,res) {
   var queryString = 'f={"_id":0}&';
   //console.log(urlMLab + 'user?'+ queryString + apikeyMLab);

   glb.httpClient.get('user?' + queryString + apikeyMLab,
   function(err, respuestaMLab, body) {
       var response = {};
       if(err) {
           response = {"msg" : "Error obteniendo usuario."}
           res.status(500);
       } else {
         if(body.length > 0) {
           console.log(body.length);
           response = body;
           res.status(202);
         } else {
           response = {"msg" : "Ningún elemento 'user'."}
           res.status(404);
         }
       }
       res.send(response);
     });
}

function getUsersByID(req,res) {
  //var httpClient = requestJSON.createClient(urlMLab);
  var id = req.params.id;
  console.log("Cliente HTTP mLab GET id " + id);

  var queryString = 'q={"ID":' + id + '}&';
  var queryStrField = 'f={"_id":0}&';

  glb.httpClient.get('user?' + queryString + queryStrField + apikeyMLab,

  function(err, respuestaMLab, body) {
      var response = {};
      if(err) {
          response = {"msg" : "Error obteniendo usuario."}
          res.status(500);
      } else {
        if(body.length > 0) {
          console.log(body.length);
          response = body;
          res.status(202);
        } else {
          response = {"msg" : "Ningún elemento 'user'."}
          res.status(404);
        }
      }
      res.send(response);
    });
}

function insertUsers(req, res) {
  //var httpClient = requestJSON.createClient(urlMLab);
  console.log(" body recibido " + req.body);
  console.log(" body recibido " + req.body.first_name);

  glb.httpClient.get('user?' + apikeyMLab,
    function(err, respuestaMLab, body) {
      var response = {};
      if(err) {
          response = {"msg" : "Error obteniendo usuarios"}
          res.status(500);
          res.send(response);
      } else {
        if(body.length > 0) {
          console.log(body.length);

          let newID = body.length +1;
          let newUser = {
            "ID" : newID,
            "first_name": req.body.first_name,
            "last_name": req.body.last_name,
            "email":req.body.email,
            "password":req.body.password
          };
          //funciona colocando el urlMLab o sin
          glb.httpClient.post(urlMLab + "user?" + apikeyMLab, newUser,
              function (err, respuestaMLab, body){
                response = {"msg" : "Usuario insertado."}
                res.status(200);
                res.send(response);
              }
          );
        } else {
          response = {"msg" : "Ningún elemento en usuarios."}
          res.status(404);
          res.send(response);
        }
      }
    });
}
function updateUsers(req, res){
  //var httpClient = requestJSON.createClient(urlMLab);
  console.log("PUT => id = " + req.params.id);
  console.log("PUT => body " + req.body.first_name);
  console.log("PUT => body " + req.body.last_name);
  console.log("PUT => body " + req.body.email);
  console.log("PUT => body " + req.body.password);

  var id = req.params.id;
  var queryStringID = 'q={"ID":' + id + '}&';
  glb.httpClient.get('user?'+ queryStringID + apikeyMLab,
    function(error, respuestaMLab, body) {
     var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
     console.log(req.body);
     console.log(cambio);
     glb.httpClient.put('user?' + queryStringID + apikeyMLab, JSON.parse(cambio),
      function(error, respuestaMLab, body) {
        console.log("PUT error " + error);
        console.log("PUT body:"+ body); // body.n devuelve 1 si pudo hacer el update
       //res.status(200).send(body);
       res.send(body);
      });
    });
}

function deleteUsers(req, res) {
  //var httpClient = requestJSON.createClient(urlMLab);
  console.log("Delete => id " + req.params.id);

  var id = req.params.id;
   var queryStringID = 'q={"ID":' + id + '}&';
   glb.httpClient.get('user?' +  queryStringID + apikeyMLab,
     function(error, respuestaMLab, body){
       if (!error) {
         var respuesta = body[0];
         glb.httpClient.delete("user/" + respuesta._id.$oid +'?'+ apikeyMLab,
            function(error, respuestaMLab,body){
              res.send(body);
         });
       }
       else {
         res.status(404).send({"msg":"ID no valido"});
       }
     });
}

function login(req, res) {
  //var httpClient = requestJSON.createClient(urlMLab);
  console.log(" user =  " + req.body.email + " passw = " + req.body.password);

  var email = req.body.email;
  var pass = req.body.password;
  let queryString = 'q={"email":"' + email +'","password":"' + pass +'"}&';
  let limFilter = 'l=1&';
  glb.httpClient.get('user?'+ queryString + limFilter + apikeyMLab,
    function(error, respuestaMLab, body) {
      if (!error) {
          if (body.length == 1 ) {
            let login = '{"$set":{"logged":true}}';
            let idNumber = body[0].ID;
            console.log("LOGGIN " + login);
            //httpClient.put('user/' + body[0]._id.$oid + '?' + apikeyMLab, JSON.parse(login),
            glb.httpClient.put('user?q={"ID": ' + idNumber + '}&'+ apikeyMLab, JSON.parse(login),
              function(errorPut, resPut, bodyPut) {
                if (!errorPut)
                  console.log("FILAS ACTUALIZADAS " + bodyPut.n);
                if (bodyPut.n == 1)
                  res.send({'msg':'Login Correcto', 'user':body[0].email,'userid':body[0].ID});
                else {
                    res.status(404).send({"msg":"usuario no actualizo"});
                }
            });
          }
          else {
            res.status(404).send({"msg":"usuario no valido"});
          }
      }
      else {
        res.status(404).send({"msg":"usuario no valido"});
      }
   });
}

function logout(req, res) {
  //var httpClient = requestJSON.createClient(urlMLab);
  console.log(" user =  " + req.body.email + " passw = " + req.body.password);
  var email = req.body.email;
  var pass = req.body.password;
  let queryString = 'q={"email":"' + email +'","password":"' + pass +'"}&';
  let limFilter = 'l=1&';
  glb.httpClient.get('user?'+ queryString + limFilter + apikeyMLab,
    function(error, respuestaMLab, body) {
      if (!error) {
          if (body.length == 1 ) {
            let logout = '{"$unset":{"logged":""}}'
            let idNumber = body[0].ID;
            console.log("LOGOUT " + logout);
            //httpClient.put('user/' + body[0]._id.$oid + '?' + apikeyMLab, JSON.parse(login),
            glb.httpClient.put('user?q={"ID": ' + idNumber + '}&'+ apikeyMLab, JSON.parse(logout),
              function(errorPut, resPut, bodyPut) {
                if (!errorPut) {
                  console.log("FILAS ACTUALIZADAS " + bodyPut.n);
                  console.log("Body" + JSON.stringify(bodyPut));
                }
                if (bodyPut.n == 1)
                  res.send({'msg':'LogOut Correcto', 'user':body[0].email,'userid':body[0].ID});
                else {
                    res.status(404).send({"msg":"usuario no actualizado"});
                }
            });
          }
          else {
            res.status(404).send({"msg":"usuario no valido"});
          }
      }
      else {
        res.status(404).send({"msg":"usuario no valido"});
      }
   });
}

function getLogged(req, res) {
  //var httpClient = requestJSON.createClient(urlMLab);
  var queryStringID = 'q={"logged":' + true + '}&';
  glb.httpClient.get('user?' +  queryStringID + apikeyMLab,
    function(error, respuestaMLab, body){
      if (!error && body.length >= 1) {
        res.send(body);
      }
      else {
        res.status(404).send({"msg":"ID no valido"});
      }
    });
}

function getAccountsByUsers(req, res) {
  var id = req.params.id;
  console.log("User id " + id);

  var queryStringID = 'q={"id":' + id + '}&';  //id account es minuscula
  var queryString= 'f={"_id":0}&';
  glb.httpClient.get('account?' + queryString + queryStringID + apikeyMLab,
  function(err, respuestaMLab, body) {
      var response = {};
      if(err) {
          response = {"msg" : "Error obteniendo cuentas del usuario ." + id}
          res.status(500);
      } else {
        if(body.length > 0) {
          console.log(body.length);
          response = body;
          res.status(202);
        } else {
          response = {"msg" : "Ningún elemento en cuentas."}
          res.status(404);
        }
      }
      res.send(response);
    });
}

module.exports.getUsers = getUsers;
module.exports.getUsersByID = getUsersByID;
module.exports.insertUsers = insertUsers;
module.exports.updateUsers = updateUsers;
module.exports.deleteUsers = deleteUsers;
module.exports.login = login;
module.exports.logout = logout;
module.exports.getLogged = getLogged;
module.exports.getAccountsByUsers = getAccountsByUsers;

//CTRL + SHIFT + / para comentar bloques
