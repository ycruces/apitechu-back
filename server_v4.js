var express = require('express');
var bodyParser = require('body-parser');
var glb = require('./global');
var users = require('./controllers/users_v4');
var cors = require('cors');

/*
const requestJSON = require('request-json');
const urlMLab = "https://api.mlab.com/api/1/databases/techu38db/collections/";
var httpClient = requestJSON.createClient(urlMLab);
require("dotenv").config();
const apikeyMLab = "apiKey="+ process.env.MLAB_API_KEY;
*/
var app = express();
var port = process.env.PORT ||  3000;


app.use(bodyParser.json());


app.use(cors());
app.options("*",cors());


// GET users
app.get(glb.URL_BASE + "users", users.getUsers);

//peticion GET users con ID
app.get(glb.URL_BASE + "users/:id",users.getUsersByID);

//POST users
app.post(glb.URL_BASE + "users", users.insertUsers);

//PUT users
app.put(glb.URL_BASE + "users/:id", users.updateUsers);

// DELETE users
app.delete(glb.URL_BASE + "users/:id", users.deleteUsers);

//login para hacer login se usa POST
app.post(glb.URL_BASE + "login",users.login);

//logout
app.post(glb.URL_BASE + "logout",users.logout);

//Get users logged
app.get(glb.URL_BASE + "getLogged",users.getLogged);

//Get Accounts by users
app.get(glb.URL_BASE + "users/:id/accounts", users.getAccountsByUsers);


app.listen(port, function () {
  console.log('Servidor escuchando en el puerto : ' + port);
});
