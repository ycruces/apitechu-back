var express = require('express');
var bodyParser = require('body-parser');
var glb = require('./global');
var users = require('./controllers/users');
const requestJSON = require('request-json');
var app = express();

var port = process.env.PORT ||  3000;
const urlMLab = "https://api.mlab.com/api/1/databases/techu38db/collections/";

require("dotenv").config();
const apikeyMLab = "apiKey="+ process.env.MLAB_API_KEY;

app.use(bodyParser.json());

var httpClient = requestJSON.createClient(urlMLab);
//lo mismo del bloque de arriba pero lamando a un modulo
//app.get(glb.URL_BASE + "users", users.getUsers);
//CTRL + SHIFT + / para comentar bloques

app.get(glb.URL_BASE + 'users', function (req, res) {
    console.log("Cliente HTTP mLab creado.");
     var queryString = 'f={"_id":0}&';
     httpClient.get('user?' + queryString + apikeyMLab,
     function(err, respuestaMLab, body) {
         var response = {};
         if(err) {
             response = {"msg" : "Error obteniendo usuario. " + err}
             res.status(500);
         } else {
           if(body.length > 0) {
             console.log(body.length);
             response = body;
             res.status(202);
           } else {
             response = {"msg" : "Ningún elemento 'user'."}
             res.status(404);
           }
         }
         res.send(response);
       });
});



//peticion GET users con ID
app.get(glb.URL_BASE + "users/:id", function(req,res){
   var id = req.params.id;
   console.log("Cliente HTTP mLab GET id " + id);

   var queryString = 'q={"ID":' + id + '}&';
   var queryStrField = 'f={"_id":0}&';

   httpClient.get('user?' + queryString + queryStrField + apikeyMLab,

   function(err, respuestaMLab, body) {
       var response = {};
       if(err) {
           response = {"msg" : "Error obteniendo usuario."}
           res.status(500);
       } else {
         if(body.length > 0) {
           console.log(body.length);
           response = body;
           res.status(202);
         } else {
           response = {"msg" : "Ningún elemento 'user'."}
           res.status(404);
         }
       }
       res.send(response);
     });
});

app.get(glb.URL_BASE + "users/:id/accounts", function(req,res){
   var id = req.params.id;
   console.log("Cliente HTTP mLab creado con id " + id);

   var queryStringID = 'q={"id":' + id + '}&';
   //var queryString = 'q={"ID":' + id + '}&';
   var queryString= 'f={"_id":0}&';

   httpClient.get('account?' + queryString + queryStringID + apikeyMLab,

   function(err, respuestaMLab, body) {
       var response = {};
       if(err) {
           response = {"msg" : "Error obteniendo cuentas del usuario ." + id}
           res.status(500);
       } else {
         if(body.length > 0) {
           console.log(body.length);
           response = body;
           res.status(202);
         } else {
           response = {"msg" : "Ningún elemento en cuentas."}
           res.status(404);
         }
       }
       res.send(response);
     });
});

//GET users con Query String
// app.get(URL_BASE + "users", function (req, res) {
//   console.log(req.query.id);
//   console.log(req.query.name);
// });

//POST users

app.post(glb.URL_BASE + "users", function(req,res) {
  console.log(" body recibido " + req.body);
  console.log(" body recibido " + req.body.first_name);

  httpClient.get('user?' + apikeyMLab,
    function(err, respuestaMLab, body) {
      var response = {};
      if(err) {
          response = {"msg" : "Error obteniendo usuarios"}
          res.status(500);
          res.send(response);
      } else {
        if(body.length > 0) {
          console.log(body.length);

          let newID = body.length +1;
          let newUser = {
            "ID" : newID,
            "first_name": req.body.first_name,
            "last_name": req.body.last_name,
            "email":req.body.email,
            "password":req.body.password
          };
          //funciona colocando el urlMLab o sin
          httpClient.post(urlMLab + "user?" + apikeyMLab, newUser,
              function (err, respuestaMLab, body){
                response = {"msg" : "Usuario insertado."}
                res.status(200);
                res.send(response);
              }
          );

        } else {
          response = {"msg" : "Ningún elemento en usuarios."}
          res.status(404);
          res.send(response);
        }
      }
      //
    });
});

//PUT users

app.put(glb.URL_BASE + "users/:id", function(req,res) {
  console.log("PUT => id = " + req.params.id);
  console.log("PUT => body " + req.body.first_name);
  console.log("PUT => body " + req.body.last_name);
  console.log("PUT => body " + req.body.email);
  console.log("PUT => body " + req.body.password);

  var id = req.params.id;
  var queryStringID = 'q={"ID":' + id + '}&';
  httpClient.get('user?'+ queryStringID + apikeyMLab,
    function(error, respuestaMLab, body) {
     var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
     console.log(req.body);
     console.log(cambio);
     httpClient.put('user?' + queryStringID + apikeyMLab, JSON.parse(cambio),
      function(error, respuestaMLab, body) {
        console.log("PUT error " + error);
        console.log("PUT body:"+ body); // body.n devuelve 1 si pudo hacer el update
       //res.status(200).send(body);
       res.send(body);
      });
    });
});

/*
//PUT users 2da forma
app.put(glb.URL_BASE + "users/:id", function(req,res) {
  console.log("PUT => id = " + req.params.id);
  console.log("PUT => body " + req.body.first_name);
  console.log("PUT => body " + req.body.last_name);
  console.log("PUT => body " + req.body.email);
  console.log("PUT => body " + req.body.password);

  var id = req.params.id;
  let userBody = req.body;
  var queryString = 'q={"ID":' + id + '}&';

  httpClient.get('user?' + queryString + apikeyMLab,
    function(err, respuestaMLab, body){
      let response = body[0];
      console.log("GET " + body);
      //Actualizo campos del usuario

      let updatedUser = {
        "ID" : id,
        "first_name" : req.body.first_name,
        "last_name" : req.body.last_name,
        "email" : req.body.email,
        "password" : req.body.password
      };//Otra forma simplificada (para muchas propiedades)
      // var updatedUser = {};
      // Object.keys(response).forEach(key => updatedUser[key] = response[key]);
      // Object.keys(userBody).forEach(key => updatedUser[key] = userBody[key]);
      // PUT a mLab
      httpClient.put('user/' + response._id.$oid + '?' + apikeyMLab, updatedUser,
        function(err, respuestaMLab, body){
          var response = {};
          if(err) {
              response = {
                "msg" : "Error actualizando usuario."
              }
              res.status(500);
          } else {
            if(body.length > 0) {
              response = body;
            } else {
              response = {
                "msg" : "Usuario actualizado correctamente."
              }
              res.status(200);
            }
          }

          res.send(response);
        });
    });
});
*/
// DELETE users
app.delete(glb.URL_BASE + "users/:id", function(req,res) {
  console.log("Delete => id " + req.params.id);

  var id = req.params.id;
   var queryStringID = 'q={"ID":' + id + '}&';
   httpClient.get('user?' +  queryStringID + apikeyMLab,
     function(error, respuestaMLab, body){
       if (!error) {
         var respuesta = body[0];
         httpClient.delete("user/" + respuesta._id.$oid +'?'+ apikeyMLab,
            function(error, respuestaMLab,body){
              res.send(body);
         });
       }
       else {
         res.status(404).send({"msg":"ID no valido"});
       }
     });
});

//login para hacer login se usa POST
app.post(glb.URL_BASE + "login",function(req,res) {
  console.log(" user =  " + req.body.email + " passw = " + req.body.password);
  var email = req.body.email;
  var pass = req.body.password;
  let queryString = 'q={"email":"' + email +'","password":"' + pass +'"}&';
  let limFilter = 'l=1&';
  httpClient.get('user?'+ queryString + limFilter + apikeyMLab,
    function(error, respuestaMLab, body) {
      if (!error) {
          if (body.length == 1 ) {
            let login = '{"$set":{"logged":true}}';
            let idNumber = body[0].ID;
            console.log("LOGGIN " + login);
            //httpClient.put('user/' + body[0]._id.$oid + '?' + apikeyMLab, JSON.parse(login),
            httpClient.put('user?q={"ID": ' + idNumber + '}&'+ apikeyMLab, JSON.parse(login),
              function(errorPut, resPut, bodyPut) {
                if (!errorPut)
                  console.log("FILAS ACTUALIZADAS " + bodyPut.n);
                if (bodyPut.n == 1)
                  res.send({'msg':'Login Correcto', 'user':body[0].email,'userid':body[0].ID});
                else {
                    res.status(404).send({"msg":"usuario no actualizo"});
                }
            });
          }
          else {
            res.status(404).send({"msg":"usuario no valido"});
          }

      }
      else {
        res.status(404).send({"msg":"usuario no valido"});
      }
   });
});

//logout  2do entregable
//delete user.logged;
app.post(glb.URL_BASE + "logout",function(req,res) {
  console.log(" user =  " + req.body.email + " passw = " + req.body.password);
  var email = req.body.email;
  var pass = req.body.password;
  let queryString = 'q={"email":"' + email +'","password":"' + pass +'"}&';
  let limFilter = 'l=1&';
  httpClient.get('user?'+ queryString + limFilter + apikeyMLab,
    function(error, respuestaMLab, body) {
      if (!error) {
          if (body.length == 1 ) {
            let logout = '{"$unset":{"logged":""}}'
            let idNumber = body[0].ID;
            console.log("LOGOUT " + logout);
            //httpClient.put('user/' + body[0]._id.$oid + '?' + apikeyMLab, JSON.parse(login),
            httpClient.put('user?q={"ID": ' + idNumber + '}&'+ apikeyMLab, JSON.parse(logout),
              function(errorPut, resPut, bodyPut) {
                if (!errorPut)
                  console.log("FILAS ACTUALIZADAS " + bodyPut.n);
                if (bodyPut.n == 1)
                  res.send({'msg':'LogOut Correcto', 'user':body[0].email,'userid':body[0].ID});
                else {
                    res.status(404).send({"msg":"usuario no actualizado"});
                }
            });
          }
          else {
            res.status(404).send({"msg":"usuario no valido"});
          }

      }
      else {
        res.status(404).send({"msg":"usuario no valido"});
      }
   });
});

//Get users logged
app.get(glb.URL_BASE + "getLogged", function (req, res) {
  /*
  var userLogged = {
    first_name:"",
    last_name:""
  };
  var userLogged = new Object(); // no funciona correctamente porque el ultimo objeto reescribe a los anteriores
  */
  var queryStringID = 'q={"logged":' + true + '}&';
  httpClient.get('user?' +  queryStringID + apikeyMLab,
    function(error, respuestaMLab, body){
      if (!error && body.length >= 1) {
        res.send(body);
      }
      else {
        res.status(404).send({"msg":"ID no valido"});
      }
    });
});


app.listen(port, function () {
  console.log('Servidor escuchando en el puerto : ' + port);
});

function writeUserDataToFile(data) {
   var fs = require('fs');
   var jsonUserData = JSON.stringify(data);
   fs.writeFile("./users.json", jsonUserData, "utf8",
    function(err) { //función manejadora para gestionar errores de escritura
      if(err) {
        console.log(err);
      } else {
        console.log("Datos escritos en 'users.json'.");
      }
    })
}
